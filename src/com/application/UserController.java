package com.application;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.server.mvc.Viewable;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Path("/")
public class UserController {

	@Context
	HttpServletRequest request;

	@GET
	@Path("/")
	@Produces(MediaType.TEXT_HTML)
	public Response loginPage() {
		return Response.ok(new Viewable("/Login.jsp")).build();
	}

	@GET
	@Path("/signUp")
	@Produces(MediaType.TEXT_HTML)
	public Response signUpPage() {
		return Response.ok(new Viewable("/Signup.jsp")).build();
	}
	
	@GET
	@Path("/follow")
	@Produces(MediaType.TEXT_HTML)
	public Response followpage() {
		return Response.ok(new Viewable("/follow.jsp")).build();
	}
	
	@GET
	@Path("/Unfollow")
	@Produces(MediaType.TEXT_HTML)
	public Response Unfollowpage() {
		return Response.ok(new Viewable("/Unfollow.jsp")).build();
	}
	
	@GET
	@Path("/getFollowers")
	@Produces(MediaType.TEXT_HTML)
	public Response getfollowerspage() {
		return Response.ok(new Viewable("/getFollowers.jsp")).build();
	}
	
	@GET
	@Path("/checkin")
	@Produces(MediaType.TEXT_HTML)
	public Response checkIn () {
		return Response.ok(new Viewable("/checkin.jsp")).build();
	}
	
	@GET
	@Path("/save")
	@Produces(MediaType.TEXT_HTML)
	public Response SavePlace () {
		return Response.ok(new Viewable("/saveplace.jsp")).build();
	}
	
	@GET
	@Path("/addplace")
	@Produces(MediaType.TEXT_HTML)
	public Response addnewplace () {
		return Response.ok(new Viewable("/newplace.jsp")).build();
	}
	
	
	
	@GET
	@Path("/getlastPosition")
	@Produces(MediaType.TEXT_HTML)
	public Response getLastPositionpage() {
		return Response.ok(new Viewable("/getlastPosition.jsp")).build();
	}
	
	
	
	@GET
	@Path("/showLocation")
	@Produces(MediaType.TEXT_HTML)
	public Response showLocationPage(){
		return Response.ok(new Viewable("/ShowLocation.jsp")).build();
	}

	@POST
	@Path("/updateMyLocation")
	@Produces(MediaType.TEXT_PLAIN)
	public String updateLocation(@FormParam("lat") String lat, @FormParam("long") String lon){
		HttpSession session = request.getSession();
		Long id = (Long) session.getAttribute("id");
		String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/updatePosition";
		//String serviceUrl = "http://localhost:8080/FCISquare/rest/login";

		String urlParameters = "id=" + id + "&lat=" + lat + "&long="+ lon;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONParser parser = new JSONParser();
		JSONObject obj;
		try {
			obj = (JSONObject)parser.parse(retJson);
			Long status = (Long) obj.get("status");
			if(status == 1)
				return "Your location is updated";
			else
				return "A problem occured";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "A problem occured";
		
	}
	
	@POST
	@Path("/doNewPlace")
	@Produces(MediaType.TEXT_HTML)
	public Response addnewplace(@FormParam("placeName") String placename,
			@FormParam("description") String description , @FormParam("lat") double lat , 
			 @FormParam("long") double lon	) {
		String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/places/NewPlace";
		//String serviceUrl = "http://localhost:8080/FCISquare/rest/login";

		String urlParameters = "placename=" + placename + "&description=" + description 
				+ "&lat=" + lat + "&long=" + lon;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
	     obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("desc", obj.get("description"));
			session.setAttribute("name", obj.get("placeName"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
		//	session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("PlaceName"));
			map.put("desc", (String) obj.get("description"));

			 return Response.ok(new Viewable("/home.jsp", map)).build();
			 

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

@POST
	@Path("/docheckin")
	@Produces(MediaType.TEXT_HTML)
	public Response checkIn(@FormParam("email") String email,
			@FormParam("pass") String pass) {
		String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/ch/CheckIn";
	//	String serviceUrl = "http://localhost:8080/FCISquare/rest/login";
		String urlParameters = "email=" + email + "&pass=" + pass;
		
	String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
			"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
	    obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("desc", obj.get("description"));
			session.setAttribute("name", obj.get("placeName"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("PlaceName"));
			map.put("desc", (String) obj.get("description"));

			 return Response.ok(new Viewable("/home.jsp", map)).build();
			 
	} catch (ParseException e) {
			//TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}


	@POST
	@Path("/doLogin")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("email") String email,
			@FormParam("pass") String pass) {
	//	String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/login";
		String serviceUrl = "http://localhost:8080/FCISquare/rest/login";

		String urlParameters = "email=" + email + "&pass=" + pass;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("email", obj.get("email"));
			session.setAttribute("name", obj.get("name"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("name"));
			map.put("email", (String) obj.get("email"));

			return Response.ok(new Viewable("/home.jsp", map)).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	@POST
	@Path("/doSignUp")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("name") String name,
			@FormParam("email") String email, @FormParam("pass") String pass) {
		//String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/signUp";
		String serviceUrl = "http://localhost:8080/FCISquare/rest/signup";

		String urlParameters = "name=" + name + "&email=" + email + "&pass="
				+ pass;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("email", obj.get("email"));
			session.setAttribute("name", obj.get("name"));
			session.setAttribute("id", obj.get("id"));
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			session.setAttribute("pass", obj.get("pass"));
			Map<String, String> map = new HashMap<String, String>();

			map.put("name", (String) obj.get("name"));
			map.put("email", (String) obj.get("email"));

			return Response.ok(new Viewable("/home.jsp", map)).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}
	
	
	@POST
	@Path("/getlastPosition")
	@Produces(MediaType.TEXT_HTML)
	public Response showHomePage(@FormParam("id") int id) {
		//String serviceUrl = "http://se2firstapp-softwareeng2.rhcloud.com/FCISquare/rest/getLastPosition";
		String serviceUrl = "http://localhost:8080/FCISquare/rest/getlastPosition";

		String urlParameters = "id=" + id;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		HttpSession session = request.getSession();
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			
			session.setAttribute("lat", obj.get("lat"));
			session.setAttribute("long", obj.get("long"));
			

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	
	
	
	
	@POST
	@Path("/dosave")
	@Produces(MediaType.TEXT_PLAIN)
	public Response SavePlace (@FormParam("PID") int id1,@FormParam("UID") int id2){

		String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/places/SavePlace";

	//	String serviceUrl = "http://localhost:8080/FCISquare/rest/follow";
		
		HttpSession session = request.getSession();
        
		
		//String urlParameters = "id1=" + session.getAttribute("id") + "&id2="+ id2;
		String urlParameters = "id1=" + id1 + "&id2="+ id2;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("id2", obj.get("id2"));

			return Response.ok(new Viewable("/home.jsp")).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	@POST
	@Path("/dofollow")
	@Produces(MediaType.TEXT_PLAIN)
	public Response follow (@FormParam("id1") int id1,@FormParam("id2") int id2){

	//	String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/follow";

		String serviceUrl = "http://localhost:8080/FCISquare/rest/follow";
		
		HttpSession session = request.getSession();
        
		
		//String urlParameters = "id1=" + session.getAttribute("id") + "&id2="+ id2;
		String urlParameters = "id1=" + id1 + "&id2="+ id2;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");
		JSONObject obj = new JSONObject();
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject) parser.parse(retJson);
			session.setAttribute("id2", obj.get("id2"));

			return Response.ok(new Viewable("/home.jsp")).build();

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	@POST
	@Path("/doUnfollow")
	@Produces(MediaType.TEXT_PLAIN)
	public String Unfollow (@FormParam("id1") int id1,@FormParam("id2") int id2){
		HttpSession session = request.getSession();
		Long id= (Long) session.getAttribute("id1");

		String serviceUrl = "http://firstapp-swe2project.rhcloud.com/FCISquare/rest/Unfollow";

		//String serviceUrl = "http://localhost:8080/FCISquare/rest/follow";

		String urlParameters = "id1=" + id1 + "&id2="+ id2;
		// System.out.println(urlParameters);
		String retJson = Connection.connect(serviceUrl, urlParameters, "POST",
				"application/x-www-form-urlencoded;charset=UTF-8");


		JSONParser parser = new JSONParser();

		JSONObject obj;
		try {
			obj = (JSONObject)parser.parse(retJson);
			Long status = (Long) obj.get("status");
			if(status == 1)
				return "Your follow is done";
			else
				return "A problem occured";
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "A problem occured";
		
	}
	
	
	
	
	
	
}
